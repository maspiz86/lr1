import java.util.ArrayList;
import java.util.Scanner;


/**
 * Handles strings approaching them like productions, and manages them for a LR(1) parsing
 * 
 * Legend:
 * '-' 	to separate producer by products;
 * '/'	to separate different productions;
 * 'e'	for epsilon productions.
 * 
 * Avoid to use 'E' as non terminal symbol because it's the symbol wich augments the grammar.
 * 
 * @author Pizzi Massimo
 */
public class Parse {

	private static Scanner input;
	private static String word;
	private static String tempLhs;
	private static String tempRhs;


	/**
	 * Provides simple console interface to display the progresses
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		/*
		 * Gestione della grammatica e operazioni preliminari
		 */
		Grammar grammar = new Grammar(true);
		
		//Inizio il nuovo vettore con l'augmented
		ArrayList<String> augmented = new ArrayList<String> ();
		augmented.add("E:.");
		augmented.set(0, augmented.get(0).concat((grammar.getProdMatrix().get(0).get(0))+",�"));

		System.out.println(augmented.get(0)+"\n");
		
		//Inserisco il resto della grammatica
		for(int lhs = 0; lhs < grammar.getProdMatrix().size(); lhs++) {

			tempLhs = grammar.getProdMatrix().get(lhs).get(0);
			
			for(int rhs = 1; rhs < grammar.getProdMatrix().get(lhs).size(); rhs++) {

				tempRhs = grammar.getProdMatrix().get(lhs).get(rhs);
				augmented.add(tempLhs.concat(":."+tempRhs+","));
				
				System.out.println(augmented.get(augmented.size()-1)+"\n");
			}

		}
		
		/*
		 * Configurazione del parser
		 */
		Lr1 test = new Lr1();
		
		test.setGrammar(augmented);
		
		// ricavo i lookahead-set per ogni non-terminale della grammatica
		test.constructLookAheadSet();

		/*
		 * Costruzione degli stati
		 */
		System.out.println("###################");
		test.state = new ArrayList();
		test.state.add(new ArrayList <String>());
		
		// inizio dall'aumento della grammatica
		test.state.get(0).add(augmented.get(0));
		
		// autogenero il resto dello stato
		int automataSize = test.state.size();
		for (int stateCheck = 0; stateCheck < automataSize; stateCheck++) {
			
			ArrayList<String> currentState = test.state.get(stateCheck);
			int currentStateSize = currentState.size();

			System.out.println(currentState);

			for (int productionCheck = 0; productionCheck < currentStateSize; productionCheck++) {
				
				int productionSize = currentState.get(productionCheck).length();
				for (int dotCheck = 0; dotCheck < productionSize; dotCheck++) {
					
					// cerco il punto
					String currentProduction = currentState.get(productionCheck);
					int dotLocation = 2;
					while (currentProduction.charAt(dotLocation) != '.') {
						
						dotLocation++;
					}
					
					// controllo il carattere dopo il punto
					Character postDotChar = currentProduction.charAt(dotLocation+1);
					
					// se � un non-terminale
					if(Character.isUpperCase(postDotChar)) {
						
						String productionToAdd = new String();
						
						for(int i = 0; i < test.getGrammar().size(); i++) {
						
							// aggiungi allo stato le produzioni di quel non-terminale
							if (test.getGrammar().get(i).charAt(0) == postDotChar) {
								
								currentState.add(test.getGrammar().get(i));
							
								// per ogni aggiunta setta il corretto lookahead
								Character laChar = currentProduction.charAt(dotLocation+2);
								
								if(Character.isUpperCase(laChar)) {
									
									for(int j = 0; j < test.getGrammar().size(); j++) {
										
										if ( test.getGrammar().get(0).equals( postDotChar.toString() ) ) {
											currentState.get(currentStateSize-1).concat( test.getLookAheadSet().get(j) );
										}
									}
								}
								else {
									currentState.get(currentStateSize-1).concat(laChar.toString());
								}
							}
						}
					}
				}

				System.out.println(currentState);

			}
			automataSize = test.state.size();
			System.out.println("yes");
		}
		
		/*
		 * Lavoro sulla tabella
		 */
	}
}