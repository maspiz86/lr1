import java.util.ArrayList;

/**
 * @author Massimo Pizzi
 */

public class bigProduction {

	private ArrayList<String> lhs;
	private ArrayList<String> rhs;
	private String augmented;
	private String axiom;
	private ArrayList<String> bigProductionlhs;
	private ArrayList<String> bigProductionrhs;
	private ArrayList<String> lookahead;
	private static ArrayList<String> nonTerminals = new ArrayList<String>();
	static{
		nonTerminals.add("S");
		nonTerminals.add("A");
		nonTerminals.add("B");
		nonTerminals.add("C");
		nonTerminals.add("D");
		nonTerminals.add("E");
		nonTerminals.add("F");
		nonTerminals.add("G");
		nonTerminals.add("H");
		nonTerminals.add("I");
		nonTerminals.add("J");
		nonTerminals.add("K");
		nonTerminals.add("L");
		nonTerminals.add("M");
		nonTerminals.add("N");
		nonTerminals.add("O");
		nonTerminals.add("P");
		nonTerminals.add("Q");
		nonTerminals.add("R");
		nonTerminals.add("T");
		nonTerminals.add("U");
		nonTerminals.add("W");
		nonTerminals.add("X");
		nonTerminals.add("Y");
		nonTerminals.add("Z");
	}
	
	public bigProduction(ArrayList<String> lhs, ArrayList<String> rhs, String s, String a){
		this.lhs = lhs;
		this.rhs = rhs;
		augmented = s;
		axiom = a;
		this.construction();
	}
	
	private void construction(){
		bigProductionlhs = new ArrayList<String>();
		bigProductionrhs = new ArrayList<String>();
		lookahead = new ArrayList<String>();
		String string = "";
		for (int i = 0; i<lhs.size(); i++)
			if (rhs.get(i).equals("e")){
				bigProductionlhs.add(lhs.get(i));
				bigProductionrhs.add(".");
			}
			else for (int j = 0; j<rhs.get(i).length()+1; j++)
				{
				string = rhs.get(i).substring(0, j)+"."+rhs.get(i).substring(j);
				bigProductionrhs.add(string);
				bigProductionlhs.add(lhs.get(i));
				}
		int dim = bigProductionrhs.size();
		//INIZIALIZATION RULE
		for (int i = 0; i<dim; i++) //INIZIALIZZO IL VETTORE DEI LOOKAHEAD
			lookahead.add("");
		First first = new First(lhs, rhs);
		String A = "";
		String beta = "";
		for (int i = 0; i<dim; i++)
			if (bigProductionlhs.get(i).equals(augmented)||bigProductionlhs.get(i).equals(axiom))
				lookahead.set(i, "$");
			
		//CLOSURE RULE
		for (int i = 2; i<dim; i++) {
			
			if (!(bigProductionrhs.get(i).equals("."))||(bigProductionrhs.get(i).substring(bigProductionrhs.get(i).length()-1, bigProductionrhs.get(i).length()).equals(".")))
				
				for (int j = 0; j<bigProductionrhs.get(i).length(); j++)
					if (bigProductionrhs.get(i).substring(j, j+1).equals(".")) {
						
						if (!(j+1==bigProductionrhs.get(i).length()))
							A=bigProductionrhs.get(i).substring(j+1, j+2);
						
						if (j+2>=bigProductionrhs.get(i).length())
							beta="e";
						else beta = bigProductionrhs.get(i).substring(j+2, j+3);
						
						for (int k = 0; k<dim; k++)
							if (bigProductionlhs.get(k).equals(A))
								if (beta.equals("e")) {
									
									if (lookahead.get(k).equals(""))
										lookahead.set(k, lookahead.get(i));
									else {
										bigProductionlhs.add(bigProductionlhs.get(k));
										bigProductionrhs.add(bigProductionrhs.get(k));
										lookahead.add(lookahead.get(i));
									}
								}
								else if (this.isNullable(beta)) {
									
									String h="";
									for (int q = 0; q<rhs.size(); q++)
										if (lhs.get(q).equals(A))
											h+=first.first(rhs.get(q));
									
									for (int q = 0; q<h.length(); q++){
										if (h.charAt(q) == 'e') h=h.substring(0,q)+h.substring(q+1);
										if (nonTerminals.contains(h.charAt(q)+"")) h=h.substring(0,q)+h.substring(q+1);
									}
									if (lookahead.get(k).equals(""))
										lookahead.set(k, lookahead.get(i)+h);
									else {
										bigProductionlhs.add(bigProductionlhs.get(k));
										bigProductionrhs.add(bigProductionrhs.get(k));
										lookahead.add(lookahead.get(i)+h);
									}
								}
									 else {
										 
										 if (lookahead.get(k).equals(""))
												lookahead.set(k, first.first(beta));
											else {
												bigProductionlhs.add(bigProductionlhs.get(k));
												bigProductionrhs.add(bigProductionrhs.get(k));
												lookahead.add(first.first(beta));
											}
									 }
						}
			}
		
		
		for (int i = 0; i<bigProductionlhs.size(); i++)	{
		String p, d, l;
		p = bigProductionlhs.get(i);
		d = bigProductionrhs.get(i);
		l = lookahead.get(i);
		for (int j = i+1; j<bigProductionlhs.size(); j++)
			if(bigProductionlhs.get(j).equals(p)&&bigProductionrhs.get(j).equals(d)&&lookahead.get(j).equals(l)||lookahead.get(j).equals("")){
				bigProductionlhs.remove(j);
				bigProductionrhs.remove(j);
				lookahead.remove(j);
				j--;
			}
		}
	}
	
	ArrayList<String> get_bigProductionrhs(){
		return bigProductionrhs;
	}
	
	ArrayList<String> get_bigProductionlhs(){
		return bigProductionlhs;
	}
	
	ArrayList<String> get_lookahead(){
		return lookahead;
	}
	
	boolean isNullable(String string){
		for (int i = 0; i<rhs.size()-1; i++)
				if (lhs.get(i).equals(string))
					if (rhs.get(i).equals("e")) return true;
					else this.isNullable(rhs.get(i));
		return false;
	}
}