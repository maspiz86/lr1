import java.util.ArrayList;
import java.io.*;

import javax.swing.JFrame;

public class LR1parser {
	
	static String gg, stp, epsilon; //given grammar, string to parse, empty string
    static int lgg = 0, lstp;     //lunghezze int
    
//--------------------------------ArrayList delle produzioni-----------------------------------
private static ArrayList<Produzione> arrayProd (String gg){
	  ArrayList<Produzione> arrayProd = new ArrayList<Produzione> ();
	  String lhs ="";
	  String rhs ="";
	  
	  int i = 0;
	  while ( (i < lgg) && (gg.charAt(i)!= '.') ){
		 
	     while ( (i < lgg) && (gg.charAt(i)!= '-') ){
	         lhs = lhs.concat( String.valueOf(gg.charAt(i)) );
			 i++;
		 }
	           
	       if (gg.charAt(i) == '-'){ 
	             i=i+2;
	       
	         while ( (i < lgg) && (gg.charAt(i)!= ';')  && (gg.charAt(i)!= '.') ){
	    	   while ((i < lgg) && (gg.charAt(i)!= '|')  && (gg.charAt(i)!= ';') && (gg.charAt(i)!= '.')  ){
					rhs = rhs.concat( String.valueOf(gg.charAt(i)) );
					i++;
			   }
	    	   arrayProd.add(new Produzione(lhs, rhs));
				   rhs = "";
				   if(i < lgg  && (gg.charAt(i)== '|') ) {i++;}                      	    	      
         }
	       }  
	       lhs = "";
			   i++;
	  }
	  return arrayProd;  
}
//---------stampa produzioni------------------------------------------------------------------ 	  

private static void stampa_produzioni(ArrayList<Produzione> produzioni){
		
	   for(Produzione p : produzioni){
		p.stampa();
	   }
	
} 	

		/**
		 * @param args
		 */
		public static void main(String[] args) throws IOException {
			// TODO Auto-generated method stub
			
			BufferedReader in = new BufferedReader(new FileReader("in.txt"));
			 
			String s1 = in.readLine();
			String g = in.readLine(); //Leggo la grammatica dal file allegato
			System.out.println(g);
			String gg = g;
			String s2 = in.readLine();
			String w = in.readLine();//Leggo la parola dal file
			String stp = w + "$";
			
			lgg = gg.length() -1; //lunghezza grammatica data
			lstp = stp.length() -1;//lunghezza stringa da parsare
			 
			ArrayList<Produzione> prod = new ArrayList<Produzione>(); //Costruisco le produzioni
			prod = arrayProd(gg);
			System.out.println("\nLe produzioni della grammatica data sono: \n");
			
			stampa_produzioni (prod);//--stampa produzioni
			System.out.println("\nLa parola data e' "+w+"\n");	
			
			char axiom = gg.charAt(0);//--assioma
			
			String aug = "S";
			Produzione augmented = new Produzione (aug, axiom+"");
			prod.add(0, augmented);
			
			ArrayList<String> lhs = new ArrayList<String>();
			ArrayList<String> rhs = new ArrayList<String>();
			
			for(Produzione p : prod){
				lhs.add(p.get_lhs());
				rhs.add(p.get_rhs());
			   }
		   
		    System.out.println("La grammatica estesa, e' la seguente: \n");
		    stampa_produzioni (prod);		   
		    
		    //Istanzio l'oggetto che calcola le bigProduction
		    bigProduction big = new bigProduction(lhs, rhs, aug, axiom+"");
		    
		    //Istanzio l'oggetto che calcola la funzione first necessaria per i lookahead
		    First first = new First(lhs, rhs);
		   
		    ArrayList<String> bigPro1 = big.get_bigProductionlhs(); //Restituisce la lhs delle bigProduction
		    ArrayList<String> bigPro2 = big.get_bigProductionrhs(); //Restituisce la rhs delle bigProduction
		    ArrayList<String> look = big.get_lookahead(); //Restituisce i lookahead delle big Production
		    System.out.println("\nEcco l'insieme delle Big Productions: \n");
		    
		    for (int i = 0; i<bigPro1.size(); i++)
		    	System.out.println("q"+i+": "+bigPro1.get(i)+" -> "+bigPro2.get(i)+" {"+look.get(i)+"}");
		    Automa auto = new Automa(lhs, rhs, bigPro1, bigPro2, look, first); //Instanzio FA
		    auto.construction(); //costruzione e PSC
		    
		    //Prendo tutti gli stati dell'automa
			ArrayList<String> instates = auto.getin();
			ArrayList<String> label = auto.getlabel();
			ArrayList<String> fstates = auto.getfin();
			System.out.println("\nEcco l'automa generato: \n");
			for (int i = 0; i<instates.size(); i++)
				System.out.println("{ "+instates.get(i)+" --> "+label.get(i)+" --> "+fstates.get(i)+" }");
			
			//Prendo la Powerset piu' un ArrayList che contiene tutti gli stati indicizzati rispetto all'indice dell'ArrayList
			ArrayList<String> pin = auto.getpin();
			ArrayList<String> pla = auto.getplabel();
			ArrayList<String> pfi = auto.getpfin();
			ArrayList<String> sta = auto.getpsta();
			System.out.println("\nEcco l'automa dopo la Powerset Construction: \n");
			for (int i = 0; i<pin.size(); i++)
				System.out.println("{ "+pin.get(i)+" --> "+pla.get(i)+" --> "+pfi.get(i)+" }");
			System.out.println("\nGli stati della Powerset Construction sono: \n");
			for (int i = 0; i<sta.size(); i++)
				System.out.println("q"+i+" : {"+sta.get(i)+"}");
			ParsingTable tb = new ParsingTable(stp, lhs, rhs, pin, pla, pfi, sta, bigPro1, bigPro2, look);
			tb.construction();
			
		}

}
