import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 * @author Massimo Pizzi
 */

public class Table2 extends JPanel {
    private boolean DEBUG = false;

    public Table2(String labels, String table[][],int row,int col) {
        super(new GridLayout(1,0));
        
        String[] columnNames = new String[labels.length()]; 
        for (int i=0; i<labels.length(); i++){
        	columnNames[i]=labels.charAt(i)+"";
        }
        
        Object[][] data = new Object[row][col];
        for (int i = 0; i<row; i++){
			for (int j = 0; j<col; j++){
				data[i][j] = table[i][j];
			}
				
        }		

        final JTable jtable = new JTable(data, columnNames);
        jtable.setPreferredScrollableViewportSize(new Dimension(500, 70));
        jtable.setFillsViewportHeight(true);

        if (DEBUG) {
            jtable.addMouseListener(new MouseAdapter() {
                public void mouseClicked(MouseEvent e) {
                    printDebugData(jtable);
                }
            });
        }

        //Create the scroll pane and add the table to it.
        JScrollPane scrollPane = new JScrollPane(jtable);

        //Add the scroll pane to this panel.
        add(scrollPane);
    }

    private void printDebugData(JTable table) {
        int numRows = table.getRowCount();
        int numCols = table.getColumnCount();
        javax.swing.table.TableModel model = table.getModel();

        System.out.println("Value of data: ");
        for (int i=0; i < numRows; i++) {
            System.out.print("    row " + i + ":");
            for (int j=0; j < numCols; j++) {
                System.out.print("  " + model.getValueAt(i, j));
            }
            System.out.println();
        }
        System.out.println("--------------------------");
    }

    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    private static void createAndShowGUI() {
        //Create and set up the window.
        JFrame frame = new JFrame("Parsing Table");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

  /*      //Create and set up the content pane.
        SimpleTableDemo newContentPane = new SimpleTableDemo();
        newContentPane.setOpaque(true); //content panes must be opaque
        frame.setContentPane(newContentPane);
*/
        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }

}
