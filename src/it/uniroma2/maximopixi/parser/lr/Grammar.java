import java.util.ArrayList;
import java.util.Scanner;


/**
 * Handles strings approaching them like productions, and manages them for a LR(1) parsing
 * 
 * Legend:
 * '-' 	to separate producer by products;
 * '/'	to separate different productions;
 * 'e'	for epsilon productions.
 * 
 * Avoid to use 'E' as non terminal symbol because it's the symbol which augments the grammar.
 * 
 * @author Pizzi Massimo
 */
public class Grammar {

	private String inputGrammar;
	private Scanner input;
	
	private ArrayList<ArrayList<String>> prodMatrix;
	public ArrayList<ArrayList<String>> getProdMatrix() {
		return prodMatrix;
	}
	public void setProdMatrix(ArrayList<ArrayList<String>> prodMatrix) {
		this.prodMatrix = prodMatrix;
	}


	Grammar (boolean printMatrix) {
		
		this.inputGrammar = new String();
		
		System.out.println("\n###################################################################\n" +
				"\nType your grammar 'A-CC/C-cC/C-d'\n");

		input = new Scanner(System.in);

		this.inputGrammar = input.next();
		
		//Divide the grammar between different non-terminal productions.
		String[] productionsFormGram = this.inputGrammar.split("/");
		//The grammar results now in an [array, String] form.

		//Contruct the matrix.
		prodMatrix = new ArrayList<>();

		String[] stringsFormProduction;
		
		for (int x = 0; x < productionsFormGram.length; x++) {
			
			prodMatrix.add(new ArrayList<String>());
			
			stringsFormProduction = productionsFormGram[x].split("-");
			
			for(int y = 0; y < stringsFormProduction.length; y++)
				prodMatrix.get(x).add(stringsFormProduction[y]);
		}
		
		if(printMatrix) {

			// Prints each production position to verify input
			for (int i = 0; i < prodMatrix.size(); i++) {

				System.out.println("\n");

				for (int j = 0; j < prodMatrix.get(i).size(); j++)
					System.out.print(prodMatrix.get(i).get(j) + "\t");
			}
			
			System.out.println("\n\n");
		}
	}


	Grammar (String g, boolean printMatrix) {
		this.inputGrammar = g;
		
		//Divide the grammar between different non-terminal productions.
		String[] productionsFormGram = this.inputGrammar.split("/");
		//The grammar results now in an [array, String] form.

		//Contruct the matrix.
		prodMatrix = new ArrayList<>();

		String[] stringsFormProduction;
		
		for (int x = 0; x < productionsFormGram.length; x++) {
			
			prodMatrix.add(new ArrayList<String>());
			
			stringsFormProduction = productionsFormGram[x].split("-");
			
			for(int y = 0; y < stringsFormProduction.length; y++)
				prodMatrix.get(x).add(stringsFormProduction[y]);
		}
		
		if(printMatrix) {

			// Prints each production position to verify input
			for (int i = 0; i < prodMatrix.size(); i++) {

				System.out.println("\n");

				for (int j = 0; j < prodMatrix.get(i).size(); j++)
					System.out.print(prodMatrix.get(i).get(j) + "\t");
			}

		}
	}


	
	/**
	 * Removes unit-productions
	 * 
	 * @param p		A generic production
	 * 
	 * @return The related production without unit-p
	 */
	public static  ArrayList<String> noUnitProductions (ArrayList<String> p) {

		ArrayList<String> production = p;

		System.out.println("Unit-p check");
		
		for(int s = 1; s < production.size(); s++) {
			
			System.out.println(s);

			if(production.get(s).equals(p.get(0))) {

				production.remove(s);

				s--;
			}
		}
		return production;
	}


	/**
	 * Removes epsilon-productions
	 * 
	 * @param p		A generic production
	 * 
	 * @return The related unextended production
	 */
	public static  ArrayList<String> noEpsilonProduction (ArrayList<String> p) {

		ArrayList<String> production = p;

		System.out.println("Epsilon-p check");
		
		for(int s = 1; s < production.size(); s++) {

			String epsilon = "�";
			
			System.out.println(epsilon + production.get(s) + production.get(s).equals(epsilon));
			
			if(production.get(s).equals(epsilon)) {

				production.remove(s);

				s--;
			}
		}
		return p;
	}

	
	/**
	 * Removes both epsilon and unit productions
	 */
	public void noEpsilonNoUnit () {
		
		for(int x = 0; x < prodMatrix.size(); x++) {

			ArrayList<String> managedProduction = noEpsilonProduction(prodMatrix.get(x));

			managedProduction = noUnitProductions(managedProduction);

			prodMatrix.set(x, managedProduction);
		}
	}
}
