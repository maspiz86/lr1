import java.util.ArrayList;
/**
 * @author Massimo Pizzi
 */
public class Automa {

	private ArrayList<String> leftHandSide;
	private ArrayList<String> rightHandSide;
	private ArrayList<String> left;
	private ArrayList<String> right;
	private ArrayList<String> lookAhead;
	private ArrayList<String> instates = new ArrayList<String>();
	private ArrayList<String> label = new ArrayList<String>();
	private ArrayList<String> fstates = new ArrayList<String>();
	private First first;
	private ArrayList<String> inpower = new ArrayList<String>();
	private ArrayList<String> labpower = new ArrayList<String>();
	private ArrayList<String> fipower = new ArrayList<String>();
	private ArrayList<String> stapower = new ArrayList<String>();
	private static ArrayList<String> nonTerminals = new ArrayList<String>();
	static{
		nonTerminals.add("S");
		nonTerminals.add("A");
		nonTerminals.add("B");
		nonTerminals.add("C");
		nonTerminals.add("D");
		nonTerminals.add("E");
		nonTerminals.add("F");
		nonTerminals.add("G");
		nonTerminals.add("H");
		nonTerminals.add("I");
		nonTerminals.add("J");
		nonTerminals.add("K");
		nonTerminals.add("L");
		nonTerminals.add("M");
		nonTerminals.add("N");
		nonTerminals.add("O");
		nonTerminals.add("P");
		nonTerminals.add("Q");
		nonTerminals.add("R");
		nonTerminals.add("T");
		nonTerminals.add("U");
		nonTerminals.add("W");
		nonTerminals.add("X");
		nonTerminals.add("Y");
		nonTerminals.add("Z");
	}

	
	public Automa(ArrayList<String> l, ArrayList<String> r, ArrayList<String> lhs, ArrayList<String> rhs, ArrayList<String> lA, First f){
		this.left = l;
		this.right = r;
		this.leftHandSide = lhs;
		this.rightHandSide = rhs;
		this.lookAhead = lA;
		this.first = f;
	}
	
	
	void construction() {
				
		// Automata contruction
		for (int i = 0; i<leftHandSide.size()-1; i++)
		{
			if (leftHandSide.get(i).equals(leftHandSide.get(i+1))){
				for (int j = 0; j<rightHandSide.get(i).length()-1; j++)
					if (rightHandSide.get(i).substring(j, j+1).equals(".")){
						if (i<10){
							instates.add("q00"+i);
							label.add(rightHandSide.get(i).substring(j+1, j+2));
							fstates.add("q00"+(i+1));
						}
						else{
							instates.add("q0"+i);
							label.add(rightHandSide.get(i).substring(j+1, j+2));
							fstates.add("q0"+(i+1));
						}
					}
			}
			
		}
		
		// E-labels constructions
		String look = "";
		String A="";
		String beta ="";
		for (int i = 0; i<leftHandSide.size(); i++)
			for (int j = 0; j<rightHandSide.get(i).length()-1; j++)
				if (rightHandSide.get(i).substring(j, j+1).equals(".")){
					if (nonTerminals.contains(rightHandSide.get(i).substring(j+1, j+2)))
					{
					A = rightHandSide.get(i).substring(j+1, j+2);
					if (j+2==rightHandSide.get(i).length()) beta = "e";
						else beta = rightHandSide.get(i).substring(j+2, j+3);
					if (beta.equals("e")) look=lookAhead.get(i);
						else if (this.isnullable(beta)) {
							for (int r = i+1; r<lookAhead.size(); r++){
									if (lookAhead.get(r).contains(lookAhead.get(i)) && lookAhead.get(r).length()>lookAhead.get(i).length() && rightHandSide.get(r).startsWith("."))
									{
										if (i<10){
											instates.add("q00"+i);
											label.add("e");
											if (r<10) fstates.add("q00"+(r));
											else fstates.add("q0"+r);
										}
										else{
											instates.add("q0"+i);
											label.add("e");
											if (r<10) fstates.add("q00"+(r));
											else fstates.add("q0"+(r));
										}
									}
							}
							
						}
							else look = first.first(beta);
					
					for (int k = 0; k<leftHandSide.size(); k++)
						if (leftHandSide.get(k).equals(A)&&rightHandSide.get(k).startsWith(".")&&lookAhead.get(k).equals(look))
							if (i<10){
								instates.add("q00"+i);
								label.add("e");
								if (k<10) fstates.add("q00"+(k));
								else fstates.add("q0"+k);
							}
							else{
								instates.add("q0"+i);
								label.add("e");
								if (k<10) fstates.add("q00"+(k));
								else fstates.add("q0"+(k));
							}
					}
					
				}
		for (int i =0; i<instates.size(); i++){
			
			if (instates.get(i).length()>4) instates.set(i, "q"+instates.get(i).substring(2));
			if (fstates.get(i).length()>4) fstates.set(i, "q"+fstates.get(i).substring(2));
		}
		
		/*
		 * Powerset construction
		 */
		// Creates the empty automata
		String labels ="";
		for (int i = 0; i<label.size(); i++)
			if (!(label.get(i).equals("e")) && (!(labels.contains(label.get(i)))))
					labels+=label.get(i);
		
		// Creates the initial state of the automata
		inpower.add("q000"+this.path("", instates.get(0), "e"));
		labpower.add("A");
		String string = "";
		for (int i = 0; i<inpower.get(0).length(); i+=4)
			for (int j = 0; j<instates.size(); j++)
			if (inpower.get(0).substring(i, i+4).equals(instates.get(j))&&label.get(j).equals("A"))
				string+=fstates.get(j);
		int dim = string.length();
		String asd = ""; //contiene gli stati raggiunti con un arco != e
		for (int i = 0; i<dim; i+=4)
			asd+=this.path(string, string.substring(i, i+4), "e");
		fipower.add(asd);
		asd=""; 
		String arcs=""; //contiene gli stati raggiunti con gli e-arcs
		
		// Check all states connected to the initial state	

		for (int i = 0; i<labels.length(); i++){
			for (int j = 0; j<inpower.get(0).length(); j+=4)
				{
				for (int k = 0; k<instates.size(); k++)
					if ((inpower.get(0).substring(j, j+4).equals(instates.get(k)))&&label.get(k).equals(labels.substring(i, i+1)))
							asd+=fstates.get(k);
				if (asd.length()>1)
					for (int k = 0; k<asd.length(); k+=4)
						arcs+=this.path("", asd.substring(k, k+4), "e");
				}
			inpower.add(inpower.get(0));
			labpower.add(labels.charAt(i)+"");
			fipower.add(asd+arcs);
			asd="";
			arcs="";
			
		}
		//Questa funzione rimuove le ripetizioni degli stati singoli all'interno di uno stato della powerset
		for (int i = 0; i<fipower.size(); i++)
			if (fipower.get(i).length()>4)
				for (int j = fipower.get(i).length(); j>0; j-=4)
					if (fipower.get(i).substring(0, j-4).contains(fipower.get(i).substring(j-4, j)))
						fipower.set(i, fipower.get(i).substring(0, j-4));
		
		ArrayList<Boolean> flags = new ArrayList<Boolean>();
		for (int i = 0; i<inpower.size(); i++)
			flags.add(false);
			
		int dimention = fipower.size();
		boolean condition = true;
		for (int i = 0; i<flags.size(); i++)
			condition = condition && flags.get(i);
		
		while (!(condition)) {
			
			condition = true;
			
			for (int a = 0; a< dimention; a++)
				for (int i = 0; i<labels.length(); i++) {
					for (int j = 0; j<fipower.get(a).length(); j+=4) {
						flags.set(a, true);
						for (int k = 0; k<instates.size(); k++)
							if ((fipower.get(a).substring(j, j+4).equals(instates.get(k)))&&label.get(k).equals(labels.substring(i, i+1)))
								asd+=fstates.get(k);
						if (asd.length()>1)
							for (int k = 0; k<asd.length(); k+=4)
								arcs+=this.path("", asd.substring(k, k+4), "e");
						}
					inpower.add(fipower.get(a));
					labpower.add(labels.charAt(i)+"");
					fipower.add(asd+arcs);
					asd="";
					arcs="";
				}
			for (int i = 0; i<fipower.size(); i++)
				if (fipower.get(i).length()>4)
					for (int j = fipower.get(i).length(); j>0; j-=4)
						if (fipower.get(i).substring(0, j-4).contains(fipower.get(i).substring(j-4, j)))
							fipower.set(i, fipower.get(i).substring(0, j-4));
			for (int i = 0; i<fipower.size(); i++) {
				String in = inpower.get(i);
				String f = fipower.get(i);
				String l = labpower.get(i);
				for (int j = i+1; j<fipower.size(); j++)
					if (inpower.get(j).equals(in) && fipower.get(j).equals(f) && labpower.get(j).equals(l)){
						inpower.remove(j);
						labpower.remove(j);
						fipower.remove(j);
						j--;
					}
			}
			
			for (int i = 0; i<fipower.size(); i++)
				if (fipower.get(i).length()<1) {
					inpower.remove(i);
					fipower.remove(i);
					labpower.remove(i);
					i--;
				}
			for (int i = 0; i<fipower.size()-flags.size(); i++)
				flags.add(false);
			
			dimention = flags.size();
			for (int i = 0; i<flags.size(); i++)
				condition = condition && flags.get(i);	
		}
		
		for (int i = 0; i<fipower.size(); i++) {
			String in = inpower.get(i);
			String f = fipower.get(i);
			for (int j = i+1; j<fipower.size(); j++)
				if (inpower.get(j).equals(in) && fipower.get(j).equals(f)){
					inpower.remove(j);
					labpower.remove(j);
					fipower.remove(j);
					j--;
				}
		}
		for (int i = 0; i<fipower.size(); i++) {
			if (!(stapower.contains(inpower.get(i)))) stapower.add(inpower.get(i));
			if (!(stapower.contains(fipower.get(i)))) stapower.add(fipower.get(i)); 
		}
		
		for (int i = 0; i<inpower.size(); i++)
			for (int j = 0; j<stapower.size(); j++)
				{
				if (inpower.get(i).equals(stapower.get(j)))
					inpower.set(i, "q"+j);
				if (fipower.get(i).equals(stapower.get(j)))
					fipower.set(i, "q"+j);
				}
		
		for (int i = 0; i<inpower.size(); i++)
			if (inpower.get(i).length()==2) inpower.set(i, "q00"+inpower.get(i).substring(1));
			else inpower.set(i, "q0"+inpower.get(i).substring(1));
		
		for (int i = 0; i<fipower.size(); i++)
			if (fipower.get(i).length()==2) fipower.set(i, "q00"+fipower.get(i).substring(1));
			else fipower.set(i, "q0"+fipower.get(i).substring(1));
	}
	ArrayList<String> getin(){
		return instates;
	}
	ArrayList<String> getlabel(){
		return label;
	}
	ArrayList<String> getfin(){
		return fstates;
	}
	ArrayList<String> getpin(){
		return inpower;
	}
	ArrayList<String> getplabel(){
		return labpower;
	}
	ArrayList<String> getpfin(){
		return fipower;
	}
	ArrayList<String> getpsta(){
		return stapower;
	}
	boolean isnullable(String string){
		for (int i = 0; i<right.size()-1; i++)
				if (left.get(i).equals(string))
					if (right.get(i).equals("e")) return true;
					else this.isnullable(right.get(i));
		return false;
	}
	
	String path(String in, String state, String arc){
		
		ArrayList<String> check = new ArrayList<String>();
		for (int i = 0; i<instates.size(); i++)
			if (instates.get(i).equals(state) && label.get(i).equals(arc))
				check.add(fstates.get(i));
		
		while (check.size()>0){
			for (int j = 0; j<instates.size(); j++)
					if (instates.get(j).equals(check.get(0)) && label.get(j).equals("e") && !(check.contains(fstates.get(j))))
						check.add(fstates.get(j));
			in+=check.get(0);
			check.remove(0);
		}
		
		return in;
	}
}