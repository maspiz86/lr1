import java.util.ArrayList;


/**
 * Handles strings approaching them like productions, and manages them for a LR(1) parsing
 * 
 * Legend:
 * '-' 	to separate producer by products;
 * '/'	to separate different productions;
 * 'e'	for epsilon productions.
 * 
 * Avoid to use 'E' as non terminal symbol because it's the symbol wich augments the grammar.
 * 
 * @author Pizzi Massimo
 */
public class Lr1 {

	private ArrayList<String> grammar;
	private ArrayList<String> lookAheadSet = new ArrayList<String>();


	// un insieme di stati composti da un insieme di stringhe
	public ArrayList<ArrayList<String>> state;
	
	private String[][] parsingTable;



	/**
	 * Construct the lookahead set for a given nT symbol of the given grammar.
	 * 
	 * @param nonTerminal		the character to inspect
	 * @param grammar			the grammar to check
	 */
	public String constructLookAheadSet (char nonTerminal) {

		String lookAheadSet = new String();

		// controlla in ogni produzione della grammatica
		for(int i=0; i<this.grammar.size(); i++) {

			// se la produzione � quella giusta
			if (nonTerminal == this.grammar.get(i).charAt(0)) {

				char temp = this.grammar.get(i).charAt(3);
				// se il primo carattere del rhs � minuscolo
				if(Character.isLowerCase(temp)) {
					lookAheadSet += temp;
				}
				// se invece � maiuscolo
				else {
					// ricercane ricorsivamente il lookahead set
					lookAheadSet += ( constructLookAheadSet(temp) );
				}
			}	
		}


		return lookAheadSet;
	}


	/**
	 * Sets the lookahead-set for the grammar in the attribute
	 */
	public void constructLookAheadSet () {

		for(int i = 0; i < this.grammar.size(); i++) {

			Character c = this.grammar.get(i).charAt(0);
			lookAheadSet.add(constructLookAheadSet(c));
			System.out.println("Lookahead-set of " + c + ": " + lookAheadSet);
			lookAheadSet.clear();
		}
	}


	public ArrayList<String> getGrammar() {
		return grammar;
	}


	public void setGrammar(ArrayList<String> grammar) {
		this.grammar = grammar;
	}

	public ArrayList<String> getLookAheadSet() {
		return lookAheadSet;
	}


	public void setLookAheadSet(ArrayList<String> lookAheadSet) {
		this.lookAheadSet = lookAheadSet;
	}

	public String getCell(int row, int column) {

		return parsingTable[row][column];
	}


	public void setCell(int row, int column, String nonTerminal) {

		this.parsingTable[row][column] = nonTerminal;
	}
}


