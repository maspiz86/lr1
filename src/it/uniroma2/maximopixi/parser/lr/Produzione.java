/**
 * @author Massimo Pizzi
 */

public class Produzione {

	private String lhs; //left hand side
	private String rhs; //right hand side
	
	public Produzione(String lhs, String rhs){
		this.lhs = lhs;
		this.rhs = rhs;
	}
	
	public String get_rhs(){
		return this.rhs;
	}
	
	public String get_lhs(){
		return this.lhs;
	}
	
	public void set_lhs(String newLhs){
		this.lhs = newLhs; 
	}
	
	public void set_rhs(String newRhs){
	    this.rhs = newRhs;	
	}
	
	public void stampa(){
		System.out.println(this.lhs + " -> " + this.rhs);
	}
}
