import java.util.ArrayList;

/**
 * @author Massimo Pizzi
 */

public class First {



	private ArrayList<String> rhs;
	private ArrayList<String> lhs;
	private static ArrayList<String> nonTerminals = new ArrayList<String>();
	static{
		nonTerminals.add("S");
		nonTerminals.add("A");
		nonTerminals.add("B");
		nonTerminals.add("C");
		nonTerminals.add("D");
		nonTerminals.add("E");
		nonTerminals.add("F");
		nonTerminals.add("G");
		nonTerminals.add("H");
		nonTerminals.add("I");
		nonTerminals.add("J");
		nonTerminals.add("K");
		nonTerminals.add("L");
		nonTerminals.add("M");
		nonTerminals.add("N");
		nonTerminals.add("O");
		nonTerminals.add("P");
		nonTerminals.add("Q");
		nonTerminals.add("R");
		nonTerminals.add("T");
		nonTerminals.add("U");
		nonTerminals.add("W");
		nonTerminals.add("X");
		nonTerminals.add("Y");
		nonTerminals.add("Z");
	}
	public First(ArrayList<String> lhs, ArrayList<String> rhs){
		this.rhs = rhs;
		this.lhs = lhs;
	}
	//Calcola la funzione first rispetto all'algoritmo del libro
	String first(String string1){
		String sfirst="";
		boolean flag = false;
		if (string1.equals("e")) return "e";
		if ((!(nonTerminals.contains(string1.substring(0, 1))))) return string1;
		else{
			for (int i = 0; i<lhs.size(); i++)
				if (lhs.get(i).equals(string1.substring(0, 1)))
					if (!(nonTerminals.contains(rhs.get(i).substring(0, 1))))
						{
						sfirst=sfirst+rhs.get(i).substring(0, 1);
						flag = true;
						}
			if (!(flag)) 
				for (int i = 0; i<lhs.size(); i++)
					if (lhs.get(i).equals(string1.substring(0, 1)))
							sfirst+=this.first(rhs.get(i).substring(0, 1));
						
			return sfirst;
					
		}
	}

}
