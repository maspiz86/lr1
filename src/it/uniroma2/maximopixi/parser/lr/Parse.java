import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author Massimo Pizzi
 */

public class Parse {

	/**
	 * Provides simple console interface for parser
	 * 
	 * @param args
	 */

	private static String grammar;
	private static Scanner input;
	private static String word;

	public static void main(String[] args) {

		/*
		 * Acquire the grammar
		 */

		System.out.println("\n###################################################################\n" +
				"\nType your grammar 'S-CB_FA_FB;A-CS_FD_a;B-FS_CE_b;C-a_C;D-AA_.;E-BB_E;F-b_.'\n");

		input = new Scanner(System.in);
		grammar = new String();
		grammar = input.next();

		/*
		 * Divide the grammar in productions
		 */

		String[] compactProductions = grammar.split(";");

		/*
		 * Divide the producer by the products
		 */

		ArrayList<String> productions[] = new ArrayList[compactProductions.length + 1];

		String[] leftRight;
		String[] temp;

		for (int x = 0; x < compactProductions.length; x++) {

			productions[x] = new ArrayList<String>();

			leftRight = compactProductions[x].split("-");

			productions[x].add(leftRight[0]);

			temp = (leftRight[1]).split("_");

			for (int y = 0; y < temp.length; y++)
				productions[x].add(temp[y]);
		}

		// Print each production position to verify input
		for (int i = 0; i < productions.length-1; i++) {

			System.out.println("\n");

			for (int j = 0; j < productions[i].size(); j++)
				System.out.print(productions[i].get(j) + "\t");

		}

		System.out.println("\n");

		/*
		 * Remove epsilon and unit productions
		 */

		for(int x = 0; x < productions.length-1; x++) {

			ArrayList<String> managedProduction = Parse.noEpsilonProduction(productions[x]);

			managedProduction = Parse.noUnitProductions(managedProduction);

			productions[x] = managedProduction;
		}
		
		// Set the augmented grammar
		productions[productions.length-1] = new ArrayList<String>();
		productions[productions.length-1].add("@");
		productions[productions.length-1].add(productions[0].get(0));

		// Print each production position to verify input
		for (int i = 0; i < productions.length; i++) {

			System.out.println("\n");

			for (int j = 0; j < productions[i].size(); j++)
				System.out.print(productions[i].get(j) + "\t");

		}

		/*
		 * Define LR parsing states
		 */

		
		
		/*
		 * Construct LR parsing table
		 */
		
		
		while (true) {

			// Acquire a new word to test
			askForWord();

			
			// Test the word
			
		}
	}

	/**
	 * Removes epsilon-productions
	 * 
	 * @param p		A generic production
	 * 
	 * @return The unextended production
	 */

	private static ArrayList<String> production;
	private static String epsilon = ".";

	public static  ArrayList<String> noEpsilonProduction (ArrayList<String> p) {

		production = p;


		System.out.println("Epsilon-p check");
		for(int s = 1; s < production.size(); s++) {

			System.out.println(epsilon + production.get(s) + production.get(s).equals(epsilon));
			if(production.get(s).equals(epsilon)) {

				production.remove(s);

				s--;
			}
		}
		return p;
	}

	/**
	 * Removes unit-productions
	 * 
	 * @param p		A generic production
	 * 
	 * @return The production without unit-production
	 */
	
	public static  ArrayList<String> noUnitProductions (ArrayList<String> p) {

		production = p;


		System.out.println("Unit-p check");
		for(int s = 1; s < production.size(); s++) {

			System.out.println(s);
			if(production.get(s).equals(p.get(0))) {


				production.remove(s);

				s--;
			}
		}

		return production;
	}
	
	public static void askForWord () {
		
		System.out.println("\n_________________________________________________________\n" +
				"\nType the word you want to test\n");

		word = new String();
		word = input.next();

		System.out.println(word);
	}

}
