import java.util.ArrayList;

import javax.swing.JFrame;

/**
 * @author Massimo Pizzi
 */

public class ParsingTable {

	private String word;
	private ArrayList<String> lhs;
	private ArrayList<String> rhs;
	private ArrayList<String> pin, pla, pfi, sta, bigin, bigf, biglook;
	private static ArrayList<String> nonTerminals = new ArrayList<String>();
	static {
		nonTerminals.add("S");
		nonTerminals.add("A");
		nonTerminals.add("B");
		nonTerminals.add("C");
		nonTerminals.add("D");
		nonTerminals.add("E");
		nonTerminals.add("F");
		nonTerminals.add("G");
		nonTerminals.add("H");
		nonTerminals.add("I");
		nonTerminals.add("J");
		nonTerminals.add("K");
		nonTerminals.add("L");
		nonTerminals.add("M");
		nonTerminals.add("N");
		nonTerminals.add("O");
		nonTerminals.add("P");
		nonTerminals.add("Q");
		nonTerminals.add("R");
		nonTerminals.add("T");
		nonTerminals.add("U");
		nonTerminals.add("W");
		nonTerminals.add("X");
		nonTerminals.add("Y");
		nonTerminals.add("Z");
	}
	
	public ParsingTable(String aword, ArrayList<String> alhs, ArrayList<String> arhs, ArrayList<String> apin, ArrayList<String> apla, ArrayList<String> apfi, ArrayList<String> asta, ArrayList<String> abigin, ArrayList<String> abigf, ArrayList<String> abiglook){
		lhs = alhs;
		rhs = arhs;
		pin = apin;
		pla = apla;
		pfi = apfi;
		sta = asta;
		bigin= abigin;
		bigf = abigf;
		biglook = abiglook;
		word = aword;
	}
	
	// Constructs the parsing table
	void construction(){
		String string = "";
		for (int i = 0; i<pla.size(); i++)
			if (!(string.contains(pla.get(i)+""))) string+=pla.get(i);
		String labels = "$";
		for (int i = 0; i<string.length(); i++)
			if (nonTerminals.contains(string.charAt(i)+"")) labels = labels+string.charAt(i);
			else labels = string.charAt(i)+labels;
		
		int col = labels.length();
		int row = sta.size();
		String[][] table = new String[row][col];
		boolean conflict = false;
		for (int i = 0; i<row; i++)
			for (int j = 0; j<col; j++)
				table[i][j]="";
		
		// Fills the matrix beginning from shift and goto
		for (int i = 0; i<row; i++){
		
			for (int j = 0; j<col; j++)
			{
				for (int k = 0; k<pin.size(); k++)
				{
					
					if ((pin.get(k).equals("q00"+i) || pin.get(k).equals("q0"+i))&&(pla.get(k).equals(labels.charAt(j)+""))){
						if (!(nonTerminals.contains(pla.get(k))))
								table[i][j]=("sh "+pfi.get(k));
						else table[i][j]=pfi.get(k)+"";
					}
				}
			}
		}
		
		// Inserting "reduce"
		for (int i = 0; i<pin.size(); i++){
			
			//Se lo stato considerato contiene un complete item, devo inserire una reduce
			int n = Integer.parseInt(pin.get(i).substring(2));
			for (int y = 0; y<sta.get(n).length()-3; y+=4)
				{
					int pos = Integer.parseInt(sta.get(n).substring(y+1, y+4));

					if (bigf.get(pos).endsWith(".")){
						
						String r = bigf.get(pos).substring(0, bigf.get(pos).length()-1);
						if (r.length()==0) r = "e";
						String l = bigin.get(pos);
						int red = -1;
						for (int g = 0; g<rhs.size(); g++)
							if (lhs.get(g).equals(l) && rhs.get(g).equals(r))
								red = g;
						if (red>=0)
							for (int x = 0; x<biglook.get(pos).length(); x++)
								for (int z = 0; z<labels.length(); z++)
									if (biglook.get(pos).charAt(x)==labels.charAt(z))
										if (table[n][z].startsWith("sh")){
											//table[n][z] = "re "+red;
											conflict = true;
										}
										else table[n][z] = "re "+red;
					}
				}
		}
		
		for (int i = 0; i<pfi.size(); i++){
			int n = Integer.parseInt(pfi.get(i).substring(2));
			for (int y = 0; y<sta.get(n).length()-3; y+=4)
				{
					int pos = Integer.parseInt(sta.get(n).substring(y+1, y+4));

					if (bigf.get(pos).endsWith(".")){
						
						String r = bigf.get(pos).substring(0, bigf.get(pos).length()-1);
						if (r.length()==0) r = "e";
						String l = bigin.get(pos);
						int red = -1;
						for (int g = 0; g<rhs.size(); g++)
							if (lhs.get(g).equals(l) && rhs.get(g).equals(r))
								red = g;
						if (red>=0)
							for (int x = 0; x<biglook.get(pos).length(); x++)
								for (int z = 0; z<labels.length(); z++)
									if (biglook.get(pos).charAt(x)==labels.charAt(z))
										table[n][z] = "re "+red;
					
					}
				}
		}
		
		for (int i = 0; i<row; i++)
			for (int j = 0; j<col; j++)
				if (table[i][j].equals("re 0")) table[i][j] = "acc";
				
		if (conflict) System.out.println("La grammatica non e' LR(1) dato che genera conflitti.");

			JFrame frame = new JFrame("Parsing Table");
	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	        // Creates and sets up the content pane
	        Table2 newContentPane = new Table2(labels, table, row, col);
	        newContentPane.setOpaque(true);
	        frame.setContentPane(newContentPane);

	        // Displays the window
	        frame.pack();
	        frame.setVisible(true);
		 
		System.out.println("\nLo stack del PDA che parsa:\n");
		int state = 0;
		int l = word.length();
		int i = 0;
		boolean flag = false;
		int pointer = 0;
		String stack = "q000";
		System.out.println(stack);
		
		/*
		 *  Check if the word is generated by the grammar
		 */
		while (pointer<l){
			for (int f = 0; f<labels.length(); f++)
				if (labels.charAt(f)==word.charAt(pointer)) i = f;
			state = Integer.parseInt(stack.substring(stack.length()-2));
			String entry = table[state][i];
			if (entry.startsWith("sh")){
				i = Integer.parseInt(entry.substring(4));
				stack+=word.charAt(pointer)+entry.substring(3);
				System.out.println(stack);
				pointer++;
				state = i;
			}
			if (entry.equals("acc")){
				flag = true;
				pointer++;
			}
			if (entry.length()==0){
				break;
			}
			if (entry.startsWith("re")){
				int k = Integer.parseInt(entry.substring(3));
				int j;
				if (!(rhs.get(k).equals("e"))) j = (4*rhs.get(k).length())+rhs.get(k).length();
				else j = 0;

				String str = stack.substring(0, stack.length()-j);
				stack=str+lhs.get(k);
				int y = Integer.parseInt(str.substring(str.length()-1));
				
				for (int g = 0; g<labels.length(); g++)
					if (lhs.get(k).equals((labels.charAt(g)+"")))
						state = g;
				stack+=table[y][state];
				System.out.println(stack);
			}
					
		}
		
		if (flag) System.out.println("\nLa parola "+word.substring(0,word.length()-1)+" e' accettata!");
		else System.out.println("La parola "+word+" non e' accettata");
			
	}
}
